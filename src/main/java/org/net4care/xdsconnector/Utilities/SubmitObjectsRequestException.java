package org.net4care.xdsconnector.Utilities;

public class SubmitObjectsRequestException extends PrepareRequestException {

  private static final long serialVersionUID = -1082697840299941076L;

  public SubmitObjectsRequestException(String message) {
    super(message);
  }

  public SubmitObjectsRequestException(Throwable throwable) {
    super(throwable);
  }

  public SubmitObjectsRequestException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
